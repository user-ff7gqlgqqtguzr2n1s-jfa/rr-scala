name := "rr-scala"

version := "0.1"

scalaVersion := "2.13.3"

val AkkaHttpVersion = "10.1.12"

libraryDependencies ++= Seq(
  "org.slf4j" % "slf4j-api" % "1.7.30",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "net.logstash.logback" % "logstash-logback-encoder" % "6.4",
  "org.json4s" %% "json4s-jackson" % "3.5.5",
  "org.scalaj" %% "scalaj-http" % "2.4.2",
  "com.typesafe" % "config" % "1.4.0"
)