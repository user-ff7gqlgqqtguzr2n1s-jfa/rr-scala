**rr-scala logger library**

###### Adding rr-scala logger as a dependency to your application
1.	Add the following lines at the end of your application build.sbt file, which will be present at the root of the project

            // importing rr-scala logging wrapper github project as dependency
            lazy val root = Project("dpa-service", file(".")).dependsOn(rrScala)
            lazy val rrScala = RootProject(uri("http://repo.dfoundry.io/scm/rr/rr-scala.git#dev"))
    
2.  Reload the project to pull the latest dependency
3.  Add a property file named app_info.properties at src/main/resources folder and add the below lines:
    
        APP_NAME=<your application name> 
        LOG_LEVEL=INFO

##### How to use rr-scala logger to log messages in your application
1. Instantiate the logger instance as below:

        val logger: RRAdapter = new RRAdapterImpl(getClass.getName)

2. Using different rr logger methods:

   2.1 Available methods are: info, warn, debug and error
   
   2.2 Method signature:
          The method consists of 4 parameters,
          
            1.	rrId: String – This is a unique id to track a request/ process end to end. 
               Map rrId from the request body, if not present generate a random 4 uuid.
            2.	source: String –  Use this parameter to map either solution name (ex: cvcm, pvcm) 
                                 or service name (ex: dpa-service, scheduler-service)
            3.	message: String – Use this parameter to log any informational or exception message.
                                 example: info - “Message published to kafka: topic1”
                                          error - “Uncaught exception occurred: json parse exception”
            4.	optionalParam: Map[String, String] – Use this parameter to log any additional key-value data or pass Map.empty
   
On successful integration you should see structured logging on the **console** and in the **log file** that would be generated within a log folder at the root of your project (log/your-app-name.log)

