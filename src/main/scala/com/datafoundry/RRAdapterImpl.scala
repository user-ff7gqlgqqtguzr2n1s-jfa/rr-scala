package com.datafoundry

import org.slf4j.LoggerFactory
import org.slf4j.MDC

/**
 * @author sasikala
 * @created on 11/25/2020
 * @description wrapper class for structured logging
 * @version number v1.0
 */
class RRAdapterImpl(val className: String) extends RRAdapter {
  val logger = LoggerFactory.getLogger(className)

  override def info(rrId: String, source: String, message: String, optionalParam: Map[String, String]) = {
    addCustomLogParameters(rrId, source, optionalParam)
    logger.info(message)
    MDC.clear()
  }

  override def warn(rrId: String, source: String, message: String, optionalParam: Map[String, String]) = {
    addCustomLogParameters(rrId, source, optionalParam)
    logger.warn(message)
    MDC.clear()
  }

  override def debug(rrId: String, source: String, message: String, optionalParam: Map[String, String]) = {
    addCustomLogParameters(rrId, source, optionalParam)
    logger.debug(message)
    MDC.clear()
  }

  override def error(rrId: String, source: String, message: String, optionalParam: Map[String, String]) = {
    addCustomLogParameters(rrId, source, optionalParam)
    logger.error(message)
    MDC.clear()
  }

  private def addCustomLogParameters(rrId: String, source: String, optionalParam: Map[String, String]) = {
    /* Adding custom parameters in logging context for structured logging */
    MDC.put("rrId", rrId)
    MDC.put("source", source)
    if (optionalParam != null && optionalParam.nonEmpty) {
      for (key <- optionalParam.keySet) {
        MDC.put(key, optionalParam.getOrElse(key,""))
      }
    }
  }
}
